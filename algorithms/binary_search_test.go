package algorithms

import (
	"testing"
)

func TestSearch(t *testing.T) {
	searches := []struct {
		haystack []int
		needle   int
		index    int
	}{
		{
			haystack: []int{},
			needle:   1,
			index:    -1,
		},
		{
			haystack: []int{1},
			needle:   1,
			index:    0,
		},
		{
			haystack: []int{1, 3},
			needle:   1,
			index:    0,
		},
		{
			haystack: []int{1, 3},
			needle:   3,
			index:    1,
		},
		{
			haystack: []int{1, 3, 5},
			needle:   5,
			index:    2,
		},
		{
			haystack: []int{1, 3, 5, 7, 8, 9, 11, 13},
			needle:   9,
			index:    5,
		},
		{
			haystack: []int{1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89},
			needle:   55,
			index:    9,
		},
		{
			haystack: []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
			needle:   10,
			index:    9,
		},
	}

	tests := []struct {
		name      string
		searchFn  func([]int, int) int
		wantedNum int
	}{
		{
			name:     "Linear Search",
			searchFn: LinearSearch,
		},
		{
			name:     "Binary Search",
			searchFn: BinarySearch,
		},
		{
			name:     "Ternary Search",
			searchFn: TernarySearch,
		},
		{
			name:     "Meta Binary Search",
			searchFn: MetaBinarySearch,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			for _, s := range searches {
				got := tt.searchFn(s.haystack, s.needle)
				if got != s.index {
					t.Logf("Searching for needle %d in haystack %v", s.needle, s.haystack)
					t.Fatalf("Got index %d but wanted %d", got, s.index)
				}
			}
		})
	}
}
